#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from behave import *
import os
import yaml

class Helpers(object):

    def tirar_print(context, browser, scenario_name):
        browser.driver.execute_script("document.body.style.zoom=(top.window.screen.height-70)/Math.max(document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight);")
        path = os.path.abspath('screens')
        if not os.path.exists(path):
            os.makedirs(path)
        full_path = '%s/%s.png' % (path, scenario_name)
        browser.driver.save_screenshot(full_path)

    def carregar_ambiente(context, ambiente):
        arquivo_dados = 'features/support/config/%s.yaml' % ambiente
        dados = yaml.safe_load(open(arquivo_dados))
        return dados
