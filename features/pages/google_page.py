#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from time import sleep
# from pages.localizadores import *

class Pagina(object):
    def __init__(context, browser):
        context.browser = browser

    def acessar_url(context, url):
        context.browser.visit(url)

    def rolar_pagina(context):
        context.browser.execute_script("window.scrollTo(0, 600)")
        sleep(5)

    def get_url_atual(context):
        return context.browser.url

class Busca(Pagina):

    def preencher_pesquisa(context, filtro):
        context.browser.find_by_css('#lst-ib').fill(filtro)

    def clicar_btn_buscar(context):
        # context.browser.execute_script("document.querySelector('#tsf > div.tsf-p > div.jsb > center > input[type=\"submit\"]:nth-child(1)').click()")
        context.browser.execute_script("document.querySelector('#sbtc > div.gstl_0.sbdd_a > div:nth-child(2) > div.sbdd_b > div > ul > li:nth-child(11) > div > span:nth-child(1) > span > input').click()")
        # context.browser.find_by_css('#tsf > div.tsf-p > div.jsb > center > input[type="submit"]:nth-child(1)').click()
        return ResultadoDeBusca(context.browser)


class ResultadoDeBusca(Pagina):
    def fname(arg):
        pass
