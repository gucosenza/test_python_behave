#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from features.support.helpers import Helpers
from splinter import Browser
import selenium

helper = Helpers()

def before_all(context):

    tipo_browser = context.config.userdata.get("browser", "chrome")
    ambiente = context.config.userdata.get("ambiente", "hmg")
    context.dados = helper.carregar_ambiente(ambiente)

    if tipo_browser == 'chrome':
        context.browser = Browser('chrome', fullscreen=True, incognito=True)

    if tipo_browser == 'chrome_headless':
        context.browser = Browser('chrome', headless=True)

    if tipo_browser == 'firefox':
        context.browser = Browser('firefox', fullscreen=True)

    if tipo_browser == 'mobile':
        mobile_emulation = {"deviceName": "iPhone X"}
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_experimental_option("mobileEmulation",
                                               mobile_emulation)
        context.browser = Browser('chrome', options=chrome_options)

def after_scenario(context, scenario):
    for step in scenario.steps:
        if step.status == 'failed':
            scenario_name = scenario.name
            helper.tirar_print(context.browser, scenario_name)

def after_all(context):
    context.browser.quit()
