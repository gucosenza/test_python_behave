# -*- coding: UTF-8 -*-
# language: pt

Funcionalidade: Google

  Contexto:

  @google
  Cenário: Fazer pesquisa no site do google
    Dado que eu acesse o site do Google
    Quando eu pesquiso por "eiplus"
    Então vejo o resultado da pesquisa
