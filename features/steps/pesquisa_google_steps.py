from time import sleep
from features.pages.google_page import *

@given(u'que eu acesse o site do Google')
def step_impl(context):
    context.busca = Busca(context.browser)
    context.busca.acessar_url("http://www.google.com")

@when(u'eu pesquiso por "eiplus"')
def step_impl(context):
    context.busca.preencher_pesquisa('eiplus')
    context.browser.execute_script("document.querySelector('#tsf > div.tsf-p > div.jsb > center > input[type=\"submit\"]:nth-child(1)').click()")
    # context.resultado = context.busca.clicar_btn_buscar
    sleep(5)

@then(u'vejo o resultado da pesquisa')
def step_impl(context):
    assert True is context.browser.is_text_present("https://www.eiplus.com.br/", wait_time=1)
