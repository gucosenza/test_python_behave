# Testes em Python#

## Ambientes ##
* Desenvolvimento: []

### Instalando os requirements ###
Execute o seguinte comando dentro da raiz do projeto:
```shell
pip install -r requirements.txt
```

### Drivers necessários: ###
Instalar [chromedriver](https://sites.google.com/a/chromium.org/chromedriver/)

### Executando os testes em DEV usando Chrome###
Execute o seguinte comando dentro da raiz do projeto:
```shell
behave
```

### Executando os testes em DEV usando Firefox###
Execute o seguinte comando dentro da raiz do projeto:
```shell
behave -D browser=firefox
```

### Executando os testes em HOM usando Chrome###
Execute o seguinte comando dentro da raiz do projeto:
```shell
behave -D ambiente=hmg
```

### Executando os testes em HOM usando Firefox###
Execute o seguinte comando dentro da raiz do projeto:
```shell
behave -D ambiente=hmg -D browser=firefox
```

### Executando tags###
Execute o seguinte comando dentro da raiz do projeto:
```shell
behave --tags=TAG
```

### Executando tags e nao exibir testes skipped(-k) ###
behave -D browser=chrome --tags=google -k

### Executando url para configuracao do behave.ini ###
https://pythonhosted.org/behave/behave.html


### Referência
https://splinter.readthedocs.io/en/latest/drivers/chrome.html#splinter.driver.webdriver.chrome.WebDriver.is_text_present
https://pythonhosted.org/behave/tutorial.html
https://pyyaml.org/wiki/PyYAMLDocumentation
https://docs.python.org/3/library/unittest.html
https://github.com/svanoort/pyresttest/blob/master/advanced_guide.md
https://jenisys.github.io/behave.example/tutorials/tutorial01.html
https://pythonhosted.org/behave/tutorial.html
